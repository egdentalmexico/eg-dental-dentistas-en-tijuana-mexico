EG Dental, a well established dental clinic in Tijuana Mexico. Tijuana dentist, Dra Eva Guerrero, is a board certified  Endontist Specialist. There are specialists for dental implants, & smile makeovers. EG Dental works with US patients coming for dental work in Mexico for dental tourism.

Address: Calle Germán Gedovius 9506, Int. 305, Zona Urbana Rio, 22320 Tijuana, B.C., Mexico

Phone: 619-373-8375

Website: http://www.egdentalmex.com
